# Algora|App

This is a dummy app that can be used as a starting point to build applications
based on the modular algorithm framework
[**Algora**](https://libalgora.gitlab.io).

## Building

**Algora** is written in C++17.
Implementations are based on the STL and additionally use selected boost
libraries.
The building process employs `qmake` version 5.

On Debian/Ubuntu, all dependencies can be installed by running: `# apt install
qt5-qmake libboost-dev`.
On Fedora, run `# dnf install qt5-qtbase-devel boost-devel`.

Before you can compile the app, you need to checkout and compile the **Algora**
libraries you want to use.
Currently, only
[**Algora|Core**](https://gitlab.com/libalgora/AlgoraCore)
and
[**Algora|Dyn**](https://gitlab.com/libalgora/AlgoraDyn),
which is built on top of **Algora|Core**,
are publicly available.

To allow the build process to run smoothly, create something like the following
directory structure (omit **AlgoraDyn**, if you're not using it):
```
Algora
|- AlgoraCore
|- AlgoraDyn
|- AlgoraApp
```
This can be achieved, e.g., by running these commands:
```
$ mkdir Algora && cd Algora
$ git clone https://gitlab.com/libalgora/AlgoraCore
$ git clone https://gitlab.com/libalgora/AlgoraDyn
$ git clone https://gitlab.com/libalgora/AlgoraApp
```

Note that you need to compile **Algora|Core** (and **Algora|Dyn**, if
necessary) before you can build the app!
See the respective READMEs for further instructions.

Like the other parts of **Algora**, **Algora|App** comes with an
`easyCompile` script that creates the necessary build directories and
compiles the app on Linux.
If you have compiled **Algora|Core** (and **Algora|Dyn**), all you need to do
now is:
```
$ cd AlgoraApp
$ ./easyCompile
```
The compiled binaries can then be found in the `build/Debug` and `build/Release`
subdirectories.

Alternatively or on other OSes, you can manually run qmake on `src/AlgoraApp.pro`
or open the file in an IDE like QtCreator.

## License

**Algora** is free software and licensed under the GNU General Public License
version 3.  See also the file `COPYING`.

## Contributors

- Kathrin Hanauer (project initiator & maintainer)
